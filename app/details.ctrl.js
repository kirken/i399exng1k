(function () {
    'use strict';

    angular.module('app').controller('DetailsCtrl', Ctrl);

    function Ctrl($http, $routeParams, $location) { //url-ilt parameetri saamiseks on $routeParams.id
        var vm = this;

        vm.item = {}; //viitan, et eksiterib objekt, mis loetakse lõpuks http.get-iga

        vm.back = back;

        $http.get('api/tasks/' + $routeParams.id).then(function (result) { //id tuleb route lehelt, kus id viide
            vm.item = result.data;
        });

        function back(){
            $location.path('/list'); //vaate vahetamiseks on $location.path('/list');
        }
    }

})();

