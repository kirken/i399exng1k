(function () {
    'use strict';

    angular.module('app').controller('ListCtrl', Ctrl);

    function Ctrl($http, modalService) {
        var vm = this;
        this.myVar = 1;

        this.increase = function () {
            this.myVar++;
        };

        this.newItem = '';
        this.items = [];
        this.addNew = addNew;
        this.removeItem = removeItem;

        init();

        function init(){
            $http.get('api/tasks').then(function (result) { //resources: http://localhost:3000/tasks
                vm.items = result.data;
            }); // get(req, resp, next)
        }

        function removeItem(id) {
            //küsida enne kinnitust kustutamiseks modal service selle jaoks
            modalService.confirm()
                .then(function () {
                    return $http.delete('api/tasks/' + id);
                }).then(init);
        }

        function addNew() {
            var newItem = {
                title: this.newItem,
                added: new Date(),
                done: false
            };
            $http.post('api/tasks', newItem).then(init);
            //this.items.push(newItem);
            this.newItem = '';
        }
    }

})();

